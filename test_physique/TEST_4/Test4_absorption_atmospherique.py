#!/usr/bin/python
# -*- encoding: utf-8 -*-
"""!
Case 4: Testing the atmospheric absorption, comparison between the calculus of tympan (remade) and a reference
"""

import numpy as np
import matplotlib.pyplot as plt

from tympan.models.project import Project
from tympan.models.solver import Model, Solver

from util_mat import attenuation, FREQ_TABLE, calcul_celerity

plt.style.use("ggplot")


PI = np.pi
def_name = "default"
def_param = r"param_solver.ini"


def tympan_attenuation(param_list, solver, model, lw=np.array([100] * 31)):
    """
    Calcul de l'atténuation atmosphérique
    @param param_list array<3-tuple>: Les tuples contenant les paramètres de calcul
    @param solver Str: Nom du solver
    @param model: Modèle tympan extrait du fichier xml
    @param lw array: spectre de la source
    @out res array: spectre d'attenuation
    """
    res = []
    freq_power = 10 ** (lw / 10) * 1e-12  # array size 31
    for hygro, temp, pres in param_list:
        solver.atmos_hygrometry = hygro
        solver.atmos_temperature = temp
        solver.atmos_pressure = pres
        result = solver.solve(model).spectrum(0, 0).to_dB().values  # array size 31
        rho, celerity = calcul_celerity(pres, temp)
        power = freq_power * rho * celerity / (4 * PI)  # array size 31 #distance is 1
        res.append(10 * np.log10(power / 4e-10) - result)
    return np.array(res)


def calcul_attenuation(param_list):
    """!
    Calcul de l'attenuation atmosphérique par tympan
    @param param_list array<3-tuple>: Les tuples contenant les paramètres de calcul
    @out res array<array<int>>: Les résultats à comparer
    """
    res = []
    for hygro, temp, pres in param_list:
        res.append(attenuation(hygro, temp, pres))
    return np.array(res)


def main(file_name, param_list, solver_name=def_name, param_file=def_param, plot_mode=True):
    th_attenuation = calcul_attenuation(param_list)
    model = Model.from_project(Project.from_xml(file_name))
    lw = model.sources[0].spectrum.to_dB().values
    solver = Solver.from_name(solver_name, param_file)
    ty_attenuation = tympan_attenuation(param_list, solver, model, lw)

    if plot_mode:
        fig, (ax_th, ax_ty) = plt.subplots(2, 1, sharey=True)
        ax_th.set_title("Calcul de l'attenuation atmosphérique théorique")
        ax_ty.set_title("Calcul de l'attenuation atmosphérique de tympan")
        for theo, tymp, p in zip(th_attenuation, ty_attenuation, param_list):
            ax_th.plot(FREQ_TABLE, theo, label="hygro={},temp={},press={}".format(p[0], p[1], p[2]))
            ax_ty.plot(FREQ_TABLE, tymp, label="hygro={},temp={},press={}".format(p[0], p[1], p[2]))
        plt.savefig("output/test4.jpg")
    return th_attenuation, ty_attenuation
