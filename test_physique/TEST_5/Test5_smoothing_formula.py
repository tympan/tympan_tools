#!/usr/bin/python
# -*- coding:utf-8 -*-
"""!
Case 5: Calcul de l'effet de sol, validité de la formule de lissage
"""
from tympan.models.project import Project
from tympan.models.solver import Model, Solver

import numpy as np
import matplotlib.pyplot as plt

from util_mat import tympan_result, calcul_celerity, FREQ_TABLE, coeff_refl, attenuation, convtiers

plt.style.use("ggplot")

PI = np.pi
# default param for solver
default_file = r"param_solver.ini"
# default solver
default_solver = "default"

freq = np.array([1000 * 10 ** (i / 10) for i in range(-18, 13)])


def calcul_analytic(
    tympan_model,
    impe_sol=150,
    temperature=20,
    atm_pression=1.01325e5,
    hygro=50,
    spectrum=None,
    frequences=FREQ_TABLE,
    lissage=False,
):
    """!
    Fait le calcul théorique du résultat tympan
    @param tympan_model model: Le model tympan utilisé pour les position des sources et des récépteurs
    @param impe_sol int: la resistivité du sol
    @param temperature int: La temperature en degré celsius
    @param atm_pression int: la pression atmospherique en Pa
    @param hygro int: L'hygrometrie relative en %
    @param spectrum array: le spectre de la source, si il y a plus de 31 valeurs le résultat sera lissé
    @param frequences array: les frequences auxquels s'applique le spectre spectrum.shape == frequences.shape
    @param lissage bool:si True, on applique la formule de lissage
    @out level_total array: le niveau des récépteurs calculés analytiquement
    """
    if spectrum is None:
        spectrum = tympan_model.sources[0].spectrum.to_dB().values
    z_rec = np.array([rec.position.z for rec in tympan_model.receptors])
    z_src = np.array([src.position.z for src in tympan_model.sources])
    dist_chemin = np.array([np.zeros(tympan_model.nreceptors)] * tympan_model.nsources)
    dist_src_rec = np.array([np.zeros(tympan_model.nreceptors)] * tympan_model.nsources)
    for isrc, src in enumerate(tympan_model.sources):
        for irec, rec in enumerate(tympan_model.receptors):
            dist_sol = np.sqrt(
                (rec.position.x - src.position.x) ** 2 + (rec.position.y - src.position.y) ** 2
            )
            dist_src_rec[isrc][irec] = np.sqrt(
                (rec.position.x - src.position.x) ** 2
                + (rec.position.y - src.position.y) ** 2
                + (rec.position.z - src.position.z) ** 2
            )
            dist_chemin[isrc][irec] = np.sqrt((rec.position.z + src.position.z) ** 2 + dist_sol**2)
    attenuation_sol = coeff_refl(
        z_rec, z_src, dist_src_rec, impe_sol, temperature, atm_pression, frequences
    )  # array size nsrc,nrec,nfreq
    atte = attenuation(hygro, temperature, atm_pression, frequences)
    rho, celerity = calcul_celerity(atm_pression, temperature)
    k = 2 * frequences * PI / celerity  # array size nfreq
    freq_power = 10 ** (spectrum / 10) * 1e-12
    power_direct = np.array(
        [
            np.array([freq_power * rho * celerity / (4 * PI * d_s_r**2) for d_s_r in di_sr])
            for di_sr in dist_src_rec
        ]
    )  # array size nsrc, nrec, nfreq

    power_chemin = np.array(
        [np.array([freq_power * rho * celerity / (4 * PI * dc**2) for dc in di_c]) for di_c in dist_chemin]
    )  # array size nsrc, nrec, nfreq
    # Rayon direct = geom*atmos*exp
    geom_dir = np.sqrt(power_direct / 4e-10)
    atmos_dir = 10 ** (
        -1 * np.array([np.array([atte * dsr for dsr in d_s_r]) for d_s_r in dist_src_rec]) / 20
    )
    exp_dir = np.exp(
        complex(0, 1) * np.array([np.array([k * dsr for dsr in d_s_r]) for d_s_r in dist_src_rec])
    )
    contri_direct = geom_dir * atmos_dir * exp_dir
    # Rayon_reflechi = geom*atmos*exp*refl
    geom_refl = np.sqrt(power_chemin / 4e-10)
    atmos_refl = 10 ** (-1 * np.array([np.array([atte * dc for dc in d_c]) for d_c in dist_chemin]) / 20)
    exp_refl = np.exp(complex(0, 1) * np.array([np.array([k * dc for dc in d_c]) for d_c in dist_chemin]))
    refl = np.array(
        [
            np.array([at * rap for at, rap in zip(at_rec, rapport)])
            for at_rec, rapport in zip(attenuation_sol, dist_src_rec / dist_chemin)
        ]
    )
    contri_refl = geom_refl * atmos_refl * exp_refl * refl
    # niveau total = somme en physique
    level_total = 20 * np.log10(abs(contri_direct + contri_refl))

    if lissage:
        sCarreModule = np.array([np.zeros(31)] * tympan_model.nreceptors)
        res_int = np.array([np.zeros(31)])
        res = np.array([np.array([np.zeros(31)] * tympan_model.nreceptors)] * tympan_model.nsources)

        for irec in range(tympan_model.nreceptors):
            sCarreModule[irec] = abs(contri_direct[0][irec]) ** 2 + abs(contri_refl[0][irec]) ** 2
            res_int = abs(contri_direct[0][irec]) * abs(contri_refl[0][irec]) * 2
            res[0][irec] = (
                res_int
                * convtiers(
                    contri_direct[0][irec],
                    contri_refl[0][irec],
                    k,
                    dist_src_rec[0][irec],
                    dist_chemin[0][irec],
                )
                + sCarreModule[irec]
            )
        level_total = 10 * np.log10(abs(res))

    return level_total


def main(
    tympan_file,
    solver_name=default_solver,
    param_file=default_file,
    spectrum=None,
    frequences=FREQ_TABLE,
    plot_mode=True,
):
    """!
    @param tympan_file String: Le fichier tympan pour la simulation
    @param solver_name String: Le solver a utiliser
    @param param_file String: le fichier de parametres a utiliser
    @param spectrum array: le spectre de la source, si il y a plus de 31 valeurs le résultat sera lissé
    @param frequences array: les frequences auxquels s'applique le spectre spectrum.shape == frequences.shape
    @param plot_mode bool: True si un affichage est requis
    """
    ty_res = tympan_result(tympan_file, solver_name, param_file)
    proj = Project.from_xml(tympan_file)
    model = Model.from_project(proj)
    solver = Solver.from_name(solver_name, param_file)
    temperature, atm_pression, hygro = (
        solver.atmos_temperature,
        solver.atmos_pressure,
        solver.atmos_hygrometry,
    )
    impe_sol = proj.site.material_areas[0].ground_material.resistivity
    ana = calcul_analytic(model, impe_sol, temperature, atm_pression, hygro, spectrum[:31], freq, False)
    ana_lis = calcul_analytic(model, impe_sol, temperature, atm_pression, hygro, spectrum[:31], freq, True)

    if plot_mode:
        for irec in range(model.nreceptors):
            plt.figure()
            plt.xlabel("frequences")
            plt.ylabel("niveau en dB")
            plt.title("Valeur sur le recepteur {}".format(irec + 1))
            plt.semilogx(FREQ_TABLE, ty_res[irec], color="blue", label="Tympan")
            plt.semilogx(FREQ_TABLE, ana[0][irec], color="red", label="sans lissage")
            plt.semilogx(FREQ_TABLE, ana_lis[0][irec], color="green", label="valeurs lisses")
            #  label="valeur analytique non lissée")
            plt.legend(loc="best")
            title = "output/test5" + str(irec + 1) + ".jpg"
            plt.savefig(title)
    return ana, ana_lis, ty_res
