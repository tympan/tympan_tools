#!/usr/bin/python
# -*- coding: utf-8 -*-

from tympan.models.project import Project
from tympan.models.solver import Model, Solver
from util_mat import FREQ_TABLE

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")

default = "default"
param_solver = "param_solver.ini"


def get_ordered_tympan(tympan_file, solver, param_file):
    """!
    Recupere le résultat tympan et le filtre selon une coupe y = 0.0033, renvoie le résultat trié selon y
    @param tympan_file String: Le fichier tympan pour la simulation
    @param solver String: Le nom du solver a utiliser
    @param param_file String: Le fichier de parametres pour la simulation
    @out x array: Les positions des récépteurs
    @out spec array<array>: Les spectres des récépteurs
    """
    proj = Project.from_xml(tympan_file)
    model = Model.from_project(proj)
    solver = Solver.from_name(solver, param_file)
    result = solver.solve(model)
    y = np.array([rec.position.y for rec in model.receptors])
    x = np.array([rec.position.x for rec in model.receptors])
    spec = np.array([10 * np.log10(sum(rec) / 4e-10) for rec in result.spectra()])
    spec = np.array([sp for i, sp in sorted(zip(y, spec), key=lambda val: val[0], reverse=True)])
    x = np.array([sp for i, sp in sorted(zip(y, x), key=lambda val: val[0], reverse=True)])
    y = sorted(y, reverse=True)
    spec = np.array([val for yv, val in zip(y, spec) if yv == 0.0033333333333334936])
    x = np.array([val for yv, val in zip(y, x) if yv == 0.0033333333333334936])
    return x, spec


def main(
    tympan_file,
    csv_files,
    solver_name=default,
    param_file=param_solver,
    headers=[["", "", "", ""], ["", "", "", ""]],
    valeurs=[1000, 2000, 4000, 6300],
    plot_mode=True,
):
    """!
    Lance le test 9 de diffraction par un cube
    @param tympan_file String: Le fichier tympan pour la simulation
    @param csv_files array: La liste des fichiers csv à comparer
    @param solver String: Le solver a utiliser
    @param param_file String: Le fichier de parametres pour la simulation
    @param headers array: Les en-tete des colonnes dans les fichiers csv
    @param valeurs array: Les valeurs des fréquences à comparer
    @param plot_mode bool: True si un affichage est requis
    """
    x_ty, spec = get_ordered_tympan(tympan_file, solver_name, param_file)
    df_aec = pd.read_csv(csv_files[0], sep=";", dtype=float)
    x_th_aec = df_aec["x(m)"].values
    df_sec = pd.read_csv(csv_files[1], sep=";", dtype=float)
    x_th_sec = df_sec["x(m)"].values
    res_tymp = []
    res_th = []
    for id_fig, val in enumerate(valeurs):
        id_freq = int(np.where(FREQ_TABLE == val)[0])
        ty_res = np.array([rec[id_freq] for rec in spec])
        if plot_mode:
            plt.figure()
            plt.title("valeur a {}hz".format(val))
            plt.xlabel("distance en m")
            plt.ylabel("niveau en dB")
            plt.plot(x_ty, ty_res, color="red", label="Tympan")
            th_aec = df_aec[headers[0][id_fig]].values
            plt.plot(x_th_aec, th_aec, label=headers[0][id_fig], color="blue")
            th_sec = df_sec[headers[1][id_fig]].values
            plt.plot(x_th_sec, th_sec, label=headers[1][id_fig], color="green")
            plt.legend(loc="best")
            plt.savefig("output/test9_{}.jpg".format(val))
        res_tymp.append(ty_res)
        res_th.append([df_aec[headers[0][id_fig]].values, df_sec[headers[1][id_fig]].values])

    return np.array(res_tymp)
