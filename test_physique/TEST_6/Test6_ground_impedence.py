#!/usr/bin/python
# -*- coding: utf-8-*-

import numpy as np
import matplotlib.pyplot as plt

from tympan.models.project import Project
from tympan.models.solver import Model
from tympan.models._solver import Configuration

from util_mat import tympan_solve, coeff_refl, FREQ_TABLE, attenuation, calcul_celerity

plt.style.use("ggplot")

PI = np.pi
# default param for solver
default_file = r"param_solver.ini"
# default solver
default_solver = "default"


def get_tympan_reflexion(result, spectrum, dist3d, temperature, pressure, hygro):
    """!
    Calcul l'atténuation au sol à partir du résultat tympanique
    @param result array<array(31)> : Le résultat de la source sur le récepteur en dB
    @param spectrum array(31) : Le spectre de la source en dB
    @param dist3d int : la distance directe entre la source et le récepteur
    @param temperature int : La température de l'atmosphère en Celsius
    @param pressure int : La pression de l'atmosphère en Pa
    @param hygro int : L'hygrométrie de l'atmosphère en Pa
    """
    spec = result[0]
    assert len(spec) == 31
    atte = attenuation(hygro, temperature, pressure)
    rho, celerity = calcul_celerity(pressure, temperature)
    freq_power = 10 ** (spectrum / 10) * 1e-12
    power_direct = freq_power * rho * celerity / (4 * PI * dist3d**2)  # array size 31
    level_direct = 10 * np.log10(power_direct / 4e-10) - atte * dist3d
    reflexion = spec - level_direct
    return reflexion


def calcul_reflexive_ray(
    spectrum, dist_chemin, z_rec, z_src, dist_src_rec, temperature, pressure, hygro, res
):
    """!
    Calcul de l'infulence du chemin reflechi
    @param spectrum array(31): Le spectre de la source
    @param dist_chemin int: La distance parcourue par le rayon reflechi
    @param z_rec int: hauteur au sol du recepteur
    @param z_src int: hauteur au sol de la source
    @param dist_src_rec int: distance entre la source et le recepteur
    @param temperature int: temperature atmospherique en °C
    @param pressure int: pression atmospherique en Pa
    @param hygro int: Hygrometrie relative ne %
    @param res int: resistivité du sol en Krayls
    """
    atte = attenuation(hygro, temperature, pressure)
    rho, celerity = calcul_celerity(pressure, temperature)
    k = 2 * FREQ_TABLE * PI / celerity
    freq_power = 10 ** (spectrum / 10) * 1e-12
    power_chemin = freq_power * rho * celerity / (4 * PI * dist_chemin**2)  # array size 31
    power_direct = freq_power * rho * celerity / (4 * PI * dist_src_rec[0] ** 2)  # array size 31
    attenuation_sol = coeff_refl(z_rec, z_src, dist_src_rec, res, temperature, pressure)[0][0]
    level_total = 20 * np.log10(
        abs(
            np.sqrt(power_direct / 4e-10)
            * 10 ** (-atte * dist_src_rec[0] / 20)
            * np.exp(complex(0, 1) * dist_src_rec[0] * k)
            + np.sqrt(power_chemin / 4e-10)
            * 10 ** (-atte * dist_chemin / 20)
            * attenuation_sol
            * (dist_src_rec[0] / dist_chemin)
            * np.exp(complex(0, 1) * dist_chemin * k)
        )
    )
    level_chemin = level_total - 10 * np.log10(power_direct / 4e-10) + atte * dist_src_rec[0]
    return level_chemin


def main(
    file_name,
    solver_name=default_solver,
    param_file=default_file,
    spectrum=np.array([100] * 31),
    resistivities=[10, 32, 100, 320, 1000, 3200, 10000, 32000],
    plot_mode=True,
):
    """Lance le test 6
    @param file_name Str: Nom du fichier xml pour le résultat tympan
    @param solver_name Str: Nom du solver à utiliser
    @param param_file Str: Nom du fichier de parametres du solver
    @param spectrum array(31): Le spectre de la source
    @param resistivities array: Les resisitivités à tester
    @param plot_mode bool: True si l'affichage des courbes est requis
    """
    proj = Project.from_xml(file_name)
    model = Model.from_project(proj)
    src, rec = model.sources[0], model.receptors[0]
    z_rec = np.array([rec.position.z])
    z_src = np.array([src.position.z])
    dist_sol = (rec.position.x - src.position.x) ** 2 + (rec.position.y - src.position.y) ** 2
    dist_chemin = dist_sol + (rec.position.z + src.position.z) ** 2
    dist3d = dist_sol + (rec.position.z - src.position.z) ** 2
    dist_src_rec = np.array([np.array([dist3d])])
    config = Configuration.get()
    hygro, temperature, pressure = (
        config.getAtmosHygrometry(),
        config.getAtmosTemperature(),
        config.getAtmosPressure(),
    )
    dist_src_rec = np.array([np.array([dist3d])])
    theo_val = np.array([np.zeros(31)] * len(resistivities))
    tympan_val = np.array([np.zeros(31)] * len(resistivities))
    if plot_mode:
        fig_th, (ax_th, ax_ty) = plt.subplots(2, 1, sharey=True)
        fig_th.tight_layout(pad=5.0)
        ax_th.set_title("Courbes Theoriques")
        ax_ty.set_title("Courbes issues de tympan")
    floor = proj.site.material_areas[0].ground_material
    for ires, res in enumerate(resistivities):
        val_th = calcul_reflexive_ray(
            spectrum,
            np.sqrt(dist_chemin),
            z_rec,
            z_src,
            np.sqrt(dist_src_rec),
            temperature,
            pressure,
            hygro,
            res,
        )
        theo_val[ires] = val_th
        floor.set_resistivity(res)
        result = tympan_solve(Model.from_project(proj), solver_name, param_file)
        tymp_val = get_tympan_reflexion(result, spectrum, np.sqrt(dist_sol), temperature, pressure, hygro)
        tympan_val[ires] = tymp_val
        if plot_mode:
            plt.xlabel("frequences")
            plt.ylabel("valeur en dB")
            ax_th.plot(FREQ_TABLE, val_th, label="R={}".format(res))
            ax_ty.plot(FREQ_TABLE, tymp_val, label="R={}".format(res))
            plt.xlabel("frequences")
            plt.ylabel("valeur en dB")
    if plot_mode:
        plt.tight_layout()
        ax_ty.legend(loc="lower right", fancybox=True, ncol=3, shadow=True)
        plt.savefig("output/test6.jpg")
    return tympan_val
