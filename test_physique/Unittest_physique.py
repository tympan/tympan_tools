#!/usr/bin/python
# -*- coding: utf-8

import unittest
import numpy as np
import os
from pathlib import Path

from util_mat import FREQ_TABLE

from TEST_1.Test1_free_field import main as test1

# from TEST_2.Test2_radiating_source_cube import main as test2
# TODO : test 3
from TEST_3.Test3_building import main as test3
from TEST_4.Test4_absorption_atmospherique import main as test4
from TEST_5.Test5_smoothing_formula import main as test5
from TEST_6.Test6_ground_impedence import main as test6
from TEST_7.Test7_impedence_mixte import main as test7
from TEST_8.Test8_diffraction_ecran import main as test8
from TEST_9.Test9_diffraction_cube import main as test9
from TEST_10.Test10_diffraction_butte import main as test10
from matplotlib.colors import LinearSegmentedColormap

colors = ["blue", "green", "yellow", "red"]
nodes = [0.0, 0.33, 0.66, 1.0]  # Position of each color
cmap = LinearSegmentedColormap.from_list("custom_colormap", list(zip(nodes, colors)))
# solver = "Anime3D"
solver = "default"


class Test_physique(unittest.TestCase):
    """!
    Classe Test physique utilisant le framework de tests unitaires unittest
    """

    solver = "default"

    def test_propagation_libre(self):
        """!
        Test 1 Propagation en champ libre
        On compare les résultats avec une tolérance à 10^-1dB pour les comparaisons sur toutes les distances
        """
        print("Test 1")

        ty_2pi, ty_4pi, ty_face, ty_cube, ana_4pi, ana_2pi = test1(
            r"TEST_1\SP_2pi.xml",
            r"TEST_1\SP_4pi.xml",
            r"TEST_1\Cube.xml",
            r"TEST_1\Face.xml",
            solver_name=solver,
            param_file=r"TEST_1\param_solver.ini",
            plot_mode=True,
        )

        res1 = res2 = res3 = res4 = True

        for i in range(1, len(ty_2pi)):
            res1 = ty_2pi[i - 1] > ty_2pi[i] and res1
            res2 = ty_4pi[i - 1] > ty_4pi[i] and res2
            res3 = ty_face[i - 1] > ty_face[i] and res3
            res4 = ty_cube[i - 1] > ty_cube[i] and res4

        # On effectue la comparaison uniquement pour les résultats suffisament éloignés des sources
        res1 = res1 and np.allclose(ty_2pi[198:], ty_face[198:], atol=0.1, equal_nan=True)
        res2 = res2 and np.allclose(ty_2pi[198:], ana_2pi[198:], atol=0.1, equal_nan=True)
        res3 = res3 and np.allclose(ty_4pi[198:], ty_cube[198:], atol=0.1, equal_nan=True)
        res4 = res4 and np.allclose(ty_4pi[198:], ana_4pi[198:], atol=0.1, equal_nan=True)

        if res1 is True and res2 is True and res3 is True and res4 is True:
            print(r"Test 1 : OK")
        else:
            print(r"Test 1 : Non OK")

    def test_puissance_accoustique(self):
        """!
        Test 3 puissance accoustique d'un batiment industriel
        On effectue une non régression à partir des résultats de la version précédante
        """
        print("Test 3")
        test3(r"TEST_3\test_batiment.xml", r"TEST_3\res_test")
        oldres = np.zeros(31)
        with open(Path(str(os.getcwd()) + r"\TEST_3\res_parSurface.txt"), "r") as f:
            index = 2
            i = 0
            for line in f:
                level = line.split(";")
                if "." in level[index]:
                    oldres[i] = level[index]
                    i += 1

        newres = np.zeros(31)
        with open(Path(str(os.getcwd()) + r"\TEST_3\res_test_parSurface.txt"), "r") as f:
            index = 2
            i = 0
            for line in f:
                level = line.split(",")
                if "." in level[index]:
                    newres[i] = level[index]
                    i += 1

        res = np.allclose(oldres, newres, atol=0.01)

        if res is True:
            print(r"Test 3 : OK")
        else:
            print(r"Test 3 : Non OK")

    def test_absorption_atmospherique(self):
        """!
        Test4 absorption atmospherique
        On compare les résultats de tympan avec un calcul analytique
        avec une tolérance a 2% sur tout le spectre
        """
        print("Test 4")
        param_list = np.array(
            [(100, t, 1.01325e5) for t in [0, 10, 20, 30]] + [(50, t, 1.01325e5) for t in [0, 10, 20, 30]]
        )
        th_attenuation, ty_attenuation = test4(
            r"TEST_4\SP_1m.xml",
            param_list,
            solver_name=solver,
            param_file=r"TEST_4\param_solver.ini",
            plot_mode=True,
        )
        res = True
        for theo, tymp in zip(th_attenuation, ty_attenuation):
            res = res and np.allclose(theo, tymp, rtol=2e-2)

        if res is True:
            print(r"Test 4 : OK")
        else:
            print(r"Test 4 : Non OK")

    def test_lissage(self):
        """!
        Test5 formule de lissage
        On compare les résultats de tympan avec un calcul analytique
        avec une tolerance de 1% en autorisant au plus 1 résultat faux
        """
        print("Test 5")
        spec = np.array([100] * (31 * 20))
        f1 = []
        for freq in FREQ_TABLE:
            for i in range(1, 21):
                f1.append(2 ** (i / (3 * 21) - 1 / 6) * freq)
        _, an_lis, ty_res = test5(
            r"TEST_5\Effetsol3pointsAsphalte.xml",
            solver_name=solver,
            param_file=r"TEST_5\param_solver.ini",
            spectrum=spec,
            frequences=np.array(f1),
            plot_mode=True,
        )
        res = [True] * len(ty_res)
        for analytique, tympan, j in zip(an_lis[0], ty_res, range(len(ty_res))):
            # On autorise jusqu'a 1 faux
            compt = 0
            for i in range(31):
                if (tympan[i] - analytique[i]) / analytique[i] > 1e-2:
                    compt += 1
            res[j] = compt < 2
        if False in res:
            print(r"TEST5 : Non OK")
        else:
            print(r"TEST5 : OK")

    def test_absorption_sol(self):
        """!
        Test6 impedance du sol
        On compare les résultats de TYMPAN avec ceux de la version précédante
        """
        print("Test 6")
        tymp = test6(
            r"TEST_6\Embleton fig2.xml",
            solver_name=solver,
            param_file=r"TEST_6\param_solver.ini",
            plot_mode=True,
        )

        with open(Path(str(os.getcwd()) + r"\TEST_6\ref_res_6.txt"), "r") as txt_file:
            old_res = []
            for line in txt_file:
                for txt in line.split():
                    old_res.append(float(txt))
            res = np.allclose(tymp.flatten(), old_res, atol=1e-2)

        if res is False:
            print(r"TEST6 : Non OK")
        else:
            print(r"TEST6 : OK")

    def test_impedance_mixte(self):
        """!
        Test7 impedance mixte sol
        On compare les résultats de tympan avec les courbes théoriques de sols
        avec différentes atténuations. On accepte un écart de 0.2 à 0.3 dB
        Les courbes sont tirées des fichiers csv
        """
        print("Test 7")

        x_ty, spec, x_th, v1, v2, v3 = test7(
            r"TEST_7\EffetsolMixte2.xml",
            r"TEST_7\analytic_data_ground2.csv",
            solver_name=solver,
            param_file=r"TEST_7\param_solver.ini",
            headers=["EP", "156 kRayls", "30000 kRayls"],
            plot_mode=True,
        )

        spec_int = np.interp(x_th, x_ty, spec)

        # index correspondant au changement d'impedance
        ind1 = np.where(x_th > 59.5)[0][0]
        ind2 = np.where(x_th > 60.5)[0][0]
        ind3 = np.where(x_th > 79.5)[0][0]
        ind4 = np.where(x_th > 80.5)[0][0]

        res1 = np.allclose(spec_int[150:ind1], v2[150:ind1], atol=0.2)
        res2 = np.allclose(spec_int[ind2:ind3], v3[ind2:ind3], atol=0.2)
        res3 = np.allclose(spec_int[ind4:], v2[ind4:], atol=0.2)

        x_ty, spec, x_th, v1, v2, v3 = test7(
            r"TEST_7\EffetsolMixte.xml",
            r"TEST_7\analytic_data_ground2.csv",
            solver_name=solver,
            param_file=r"TEST_7\param_solver.ini",
            headers=["EP", "156 kRayls", "30000 kRayls"],
            plot_mode=True,
        )

        spec_int = np.interp(x_th, x_ty, spec)

        # index correspondant au changement d'impedance
        ind1 = np.where(x_th > 59.5)[0][0]
        ind2 = np.where(x_th > 60.5)[0][0]

        res4 = np.allclose(spec_int[150:ind1], v3[150:ind1], atol=0.3)
        res5 = np.allclose(spec_int[ind2:], v2[ind2:], atol=0.2)

        if res1 and res2 and res3 and res4 and res5 is True:
            print("TEST 7 : OK")
        else:
            print("TEST7 : Non OK")

    def test_diffraction_ecran(self):
        """!
        Test8 diffraction ecran mince
        On compare les résultats de TYMPAN avec ceux de la version précédante
        Les courbes sont tirées des fichiers csv
        """
        print("Test 8")
        res_tymp = test8(
            r"TEST_8\ParkingAESS2.xml",
            [r"TEST_8\analytic_data_with_screen.csv", r"TEST_8\analytic_data_without_screen.csv"],
            solver_name=solver,
            param_file=r"TEST_8\param_solver.ini",
            headers=[
                ["1000hz ae", "2000hz ae", "4000hz ae", "6300hz ae"],
                ["1000hz se", "2000hz se", "4000hz se", "6300hz se"],
            ],
            plot_mode=True,
        )
        with open(Path(str(os.getcwd()) + r"\TEST_8\ref_res_8.txt"), "r") as txt_file:
            old_res = []
            for line in txt_file:
                for txt in line.split():
                    old_res.append(float(txt))
            res = np.allclose(np.array(res_tymp).flatten(), old_res, atol=1e-2)

        if res is True:
            print("TEST 8 : OK")
        else:
            print("TEST 8 : Non OK")

    def test_diffraction_cube(self):
        """!
        Test9 diffraction cube
        On verifie etre entre les deux courbes theoriques
        on limite le nombre de discontinuité
        on verifie la decroissance lineaire
        """
        print("Test 9")
        res_tymp = test9(
            r"TEST_9\ParkingAE.xml",
            [r"TEST_9\analytic_data_with_cube.csv", r"TEST_9\analytic_data_without_cube.csv"],
            solver_name=solver,
            param_file=r"TEST_9\param_solver.ini",
            headers=[
                ["1000hz ac", "2000hz ac", "4000hz ac", "6300hz ac"],
                ["1000hz sc", "2000hz sc", "4000hz sc", "6300hz sc"],
            ],
            plot_mode=True,
        )

        with open(Path(str(os.getcwd()) + r"\TEST_9\ref_res_9.txt"), "r") as txt_file:
            old_res = []
            for line in txt_file:
                for txt in line.split():
                    old_res.append(float(txt))
            res = np.allclose(res_tymp.flatten(), old_res, atol=1e-2)

        if res is True:
            print("TEST 9 : OK")
        else:
            print("TEST 9 : Non OK")

    def test_butte(self):
        """!
        On compare le resultat de la diffraction par une butte et par une construction
        On compare directement valeurs a valeurs quand cela est possible (x > = 14)
        On verifie la symetrie des courbes
        On verifie que hors les discontinuités les courbes sont proches
        On limite le nombre de discontinuités
        """
        print("Test 10")
        res_butte, res_const = test10(
            [r"TEST_10\Butte 1.xml", r"TEST_10\Construction 1.xml"],
            solver_name=solver,
            param_file=r"TEST_10\param_solver.ini",
            plot_mode=True,
        )
        res = [False, False, False, False, False, False]
        files = [r"\TEST_10\ref_butte_100.txt", r"\TEST_10\ref_butte_500.txt", r"\TEST_10\ref_butte_1000.txt"]
        for i in range(len(files)):
            with open(Path(str(os.getcwd()) + files[i]), "r") as txt_file:
                ref_res = []
                for line in txt_file:
                    for txt in line.split():
                        ref_res.append(float(txt))
                res[i] = np.allclose(res_butte[i].flatten(), ref_res, atol=1e-3)
        files = [
            r"\TEST_10\ref_constr_100.txt",
            r"\TEST_10\ref_constr_500.txt",
            r"\TEST_10\ref_constr_1000.txt",
        ]
        for i in range(len(files)):
            with open(Path(str(os.getcwd()) + files[i]), "r") as txt_file:
                ref_res = []
                for line in txt_file:
                    for txt in line.split():
                        ref_res.append(float(txt))
                res[i + 3] = np.allclose(res_const[i].flatten(), ref_res, atol=1e-3)

        if False in res:
            print("Test 10 : Non OK")
        else:
            print("Test 10 : OK")


def main():
    tp = Test_physique()
    tp.test_propagation_libre()  # test 1
    tp.test_puissance_accoustique()  # test 3
    tp.test_absorption_atmospherique()  # test 4
    tp.test_lissage()  # test 5
    tp.test_absorption_sol()  # test 6
    tp.test_impedance_mixte()  # test 7
    tp.test_diffraction_ecran()  # test 8
    tp.test_diffraction_cube()  # test 9
    tp.test_butte()  # test 10


main()
