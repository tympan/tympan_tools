#!/usr/bin/python/
# -*-coding: utf-8 -*-

from tympan.models.project import Project
from tympan.models.solver import Model, Solver

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")


def getfreq_tympan(tympan_file, solver="default", param_file="param_solver.ini"):
    """!
    Calcule le résultat Tympan et récupère la fréquence 500hz sur chaque récepteur, renvoie la liste des
     récepteurs triés par x croissant
    @param tympan_file String: Le fichier Tympan où calculer le résultat
    @param solver String: Le solver à utiliser pour calculer le résultat
    @param param_file: Le fichier de paramètres solver utilisés pour calculer le résultat
    """
    proj = Project.from_xml(tympan_file)
    model = Model.from_project(proj)
    solver = Solver.from_name(solver, param_file)
    x = np.array([rec.position.x for rec in model.receptors])
    result = solver.solve(model)
    spec = np.array(
        [10 * np.log10(sum(rec) / 4e-10)[15] for rec in result.spectra()]
    )  # on récupère la fréquence 500Hz
    spec = np.array([sp for i, sp in sorted(zip(x, spec))])
    x.sort()
    return x, spec


def main(
    tympan_file,
    csv_file,
    solver_name="default",
    param_file="param_solver.ini",
    headers=["", "", ""],
    plot_mode=True,
):
    """!
    Lance le test numero 7
    @param tympan_file String: Le fichier tympan où calculer le résultat
    @param csv_file String: Le fichier csv où recuperer les résultats analytiques
    @param solver_name String: Le solver a utiliser pour calculer le résultat
    @param param_file: Le fichier de paramètres solver utilisés pour calculer le résultat
    @param headers array<String>: Liste des en-tete des colonnes du fichier csv
    @param plot_mode bool: Si True des courbes seront affichés
    """
    x_ty, spec = getfreq_tympan(tympan_file, solver_name, param_file)
    df_csv = pd.read_csv(csv_file, sep=";", dtype=float)
    x_th = df_csv["x(m)"].values
    v1 = df_csv[headers[0]].values
    v2 = df_csv[headers[1]].values
    v3 = df_csv[headers[2]].values
    if plot_mode:
        plt.figure()
        plt.xlabel("distance en m")
        plt.ylabel("Niveau a 500 Hz")
        plt.plot(x_ty, spec, color="red", label="valeurs tympan")
        plt.plot(x_th, v1, color="blue", label=headers[0])
        plt.plot(x_th, v2, color="green", label=headers[1])
        plt.plot(x_th, v3, color="black", label=headers[2])
        plt.legend(loc="best")
        plt.savefig("output/test7.jpg")
    return x_ty, spec, x_th, v1, v2, v3
