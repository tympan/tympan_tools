#!/usr/bin/python
# -*- coding: utf-8 -*-

from math import sqrt, exp, cos, sin
from cmath import phase
import numpy as np
import pandas as pd
from scipy.special import erfc

from tympan.models.project import Project
from tympan.models.solver import Model, Solver, Spectrum
import tympan.solve_project as tysolve

# Gas Constant
R = 8.31441e3
# gamma
GAMMA = 1.41
# molar mass of air
MAIR = 29
# default param for solver
# default solver
default_solver = "default"
default_param = None

FREQ_TABLE = np.array(
    [
        16,
        20,
        25,
        31.5,
        40,
        50,
        63,
        80,
        100,
        125,
        160,
        200,
        250,
        315,
        400,
        500,
        630,
        800,
        1000,
        1250,
        1600,
        2000,
        2500,
        3150,
        4000,
        5000,
        6300,
        8000,
        10000,
        12500,
        16000,
    ]
)
INTER_FREQ = np.array(
    [
        14.254379490245428,
        17.959392772949968,
        22.44924096618746,
        28.061551207734325,
        35.35755452174525,
        44.898481932374921,
        56.123102415468651,
        70.7151090434905,
        89.796963864749841,
        112.2462048309373,
        140.30775603867164,
        179.59392772949968,
        224.4924096618746,
        280.61551207734328,
        353.57554521745249,
        448.98481932374921,
        561.23102415468657,
        707.15109043490497,
        897.96963864749841,
        1122.4620483093731,
        1403.0775603867162,
        1795.9392772949968,
        2244.9240966187463,
        2806.1551207734324,
        3535.7554521745251,
        4489.8481932374925,
        5612.3102415468647,
        7071.5109043490502,
        8979.696386474985,
        11224.620483093729,
        14030.775603867163,
        17959.39277294997,
    ]
)

PI = np.pi

REFERENCE_PRESSURE = 101325.0
REFERENCE_TEMPERATURE = 293.15
ABSOLUTE_ZERO = 273.15


def solve_tympan_project(project):
    # Récupération du modèle acoustique (mesh, sources, récepteurs) et du solver à partir du projet
    my_model = Model.from_project(project)
    my_solver = Solver.from_project(project, verbose=True)

    # Avant d'exécuter le solver sur le modèle acoustique, on vérifie qu'il existe au moins
    # une source et un récepteur
    tysolve._check_solver_model(my_model, project)

    try:
        # Exécution du solver sur le modèle et récupération des résultats
        result = my_solver.solve(my_model)

        # Import des résultats dans le projet Tympan
        project.import_result(my_model, result)

    except Exception as e:
        print("Erreur :", e)
    return project, result


def tympan_result(file_name, solver_name=default_solver, param_file=default_param):
    """!
    Récupère le fichier du projet et renvoie le résultat
    @param file_name String:Le fichie xml contenant le projet
    @param solver_name String: Le nom du solver utilisé
    @param param_file String: Les paramètres du solver
    @out spec array like: Contient le spectre de chaque recepteur sous forme d'un numpy array
    """
    my_project = Project.from_xml(file_name)
    my_model = Model.from_project(my_project)
    my_solver = Solver.from_name(solver_name, param_file)
    my_result = my_solver.solve(my_model)
    spec = np.array([10 * np.log10(sum(rec) / 4e-10) for rec in my_result.spectra()], dtype=float)
    return spec


def tympan_solve(model, solver_name=default_solver, param_file=default_param):
    """
    Lance un calcul TYMPAN
    @param model : model tympan
    @param solver_name : nom du solver utilisé
    @param_file : nom du fichier de paramètres utilisé
    """
    my_solver = Solver.from_name(solver_name, param_file)
    my_result = my_solver.solve(model)
    spec = np.array([10 * np.log10(sum(rec) / 4e-10) for rec in my_result.spectra()], dtype=float)
    return spec


def calcul_celerity(atm_pression=1.01325e5, temperature=20):
    """!
    Calculer le paramètre rho et la célérité de l'onde
    @param atm_pression int: La pression atmosphérique en pascal
    @param temperature int: La température en degrés Celsius
    @return rho,celerity int: Le paramètre rho et la célérité
    """
    celerity = sqrt(GAMMA * R * (temperature + 273.15) / MAIR)
    rho = GAMMA * atm_pression / (celerity**2)
    return rho, celerity


def compute_hm(tK, pr_rel, perc_hum):
    """!
    Donne la fraction de vapeur d'eau dans l'air
    @param tK int : la température relative de l'atmosphère
    @param pr_rel int : la pression relative de l'atmosphère
    @param perc_hum int : L'humidité relative de l'atmosphère
    @out h int : la fraction de vapeur d'eau dans l'air
    """

    T01 = ABSOLUTE_ZERO + 0.01  # Temperature on tripel point

    C = -6.8346 * pow((T01 / tK), 1.261) + 4.6151

    prSat_prRef = pow(10, C)

    return perc_hum * prSat_prRef * pr_rel


def attenuation(perc_hum=50, temperature=20, atm_pression=1.01325e5, frequences=FREQ_TABLE):
    """!
    Donne l'atténuation dans l'air pour les conditions données, d'après ISO9613-1
    @param perc_hum int:Pourcentage d'humidité de l'air
    @param temperature int: Température en celsius
    @param atm_pression int: Pression atmosphérique
    @param frequences array: Les fréquences auxquelles one doit calculer l'atténuation
    @out alpha array like: L'atténuation pour chacune des 31 fréquences
    """
    # frequencies
    f2 = frequences**2

    tK = temperature + ABSOLUTE_ZERO  # temperature to kelvin
    pr_rel = atm_pression / REFERENCE_PRESSURE  # relative pression
    t_rel = tK / REFERENCE_TEMPERATURE  # relative temperature

    h_molaire = compute_hm(tK, pr_rel, perc_hum)

    # relaxation frequency for dioxygene
    frO = pr_rel * (24.0 + (4.04 * 10000.0 * h_molaire * ((0.02 + h_molaire) / (0.391 + h_molaire))))

    # relaxation frequency for N
    frN = (
        pr_rel * pow(t_rel, -0.5) * (9.0 + 280.0 * h_molaire * exp((-4.170 * (pow(t_rel, (-1.0 / 3.0)) - 1))))
    )

    # Calculus of the absorption
    tA_classic = 1.84e-11 * (1.0 / pr_rel) * sqrt(t_rel)
    tA_O = 0.01275 * exp(-2239.1 / tK) * (1.0 / (frO + (f2 / frO)))
    tA_N = 0.1068 * exp(-3352.0 / tK) * (1.0 / (frN + (f2 / frN)))
    alpha = 8.686 * f2 * (tA_classic + (pow(t_rel, -2.5) * (tA_O + tA_N)))
    return alpha


def attenuation_from_csv(file_name, header=1):
    """
    Renvoie l'attenuation stockée dans un fichier csv
    @param file_name : nom du fichier csv
    """
    alpha = np.array(pd.read_csv(file_name, skiprows=header, encoding="latin-1", sep=";")["dB"])
    assert len(alpha) == 31
    return alpha


def coeff_refl(z_rec, z_src, dist_src_rec, impe_sol, temperature, atm_pression, frequences=FREQ_TABLE):
    """!
    Calcul des atténuations du au sol sur l'onde des sources au recepteurs
    @param z_rec array: altitude des recepteurs
    @param z_src array: altitude des sources
    @param dist_src_rec array<array>: Distance en 2D entre la source et chaque recepteur(nsrc,nreceptors)
    @param impe_sol int: impedence du sol en Krayls
    @param temperature int: temperature en celsius
    @param atm_pression int: Pression atmosphérique en Pa
    @out Q array size (nsrc,nrec,31): Le coefficient de reflexion du sol
    """
    nrec = len(z_rec)
    nsrc = len(z_src)
    assert dist_src_rec.shape == (nsrc, nrec)
    dist_chemin = np.array(
        [
            np.array([np.sqrt((zr + zs) ** 2 + dsr**2) for zr, dsr in zip(z_rec, di_sr_re)])
            for zs, di_sr_re in zip(z_src, dist_src_rec)
        ]
    )
    rho, celerity = calcul_celerity(atm_pression, temperature)
    pfc = 2 * PI * frequences / celerity
    e = 1
    # Calculus of the incidence angles
    phi = np.array([np.zeros(nrec)] * nsrc)  # array size (nsrc,nrec)
    for isrc, zs in enumerate(z_src):
        phi[isrc] = np.arctan((z_rec + zs) / dist_src_rec[isrc])
    # floor parameters
    impe_carac = (
        1 + 9.08 * pow(impe_sol / frequences, 0.75) + complex(0, 1) * 11.9 * pow(impe_sol / frequences, 0.73)
    )  # array size nfreq
    wave_num = pfc * (
        1 + 10.8 * pow(impe_sol / frequences, 0.7) + complex(0, 1) * 10.3 * pow(frequences / impe_sol, -0.59)
    )  # array size nfreq
    # impendence of the floor
    var = np.array(
        [np.array([np.sqrt(1 - ((pfc / wave_num) * np.cos(ang)) ** 2) for ang in src]) for src in phi]
    )  # array size nsrc,nrec,nfreq
    var2 = np.array(
        [np.array([complex(0, -1) * wave_num * e * rac for rac in va]) for va in var]
    )  # array size nsrc,nrec,nfreq
    frac = 1 / (np.tanh(var2) * var)  # array size nsrc,nrec,nfreq
    floor_impe = np.array(
        [np.array([impe_carac * fr for fr in fra]) for fra in frac]
    )  # array size nsrc,nrec,nfreq
    # flat wave
    Rp = np.array(
        [
            np.array([(f_i * np.sin(ang) - 1) / (f_i * np.sin(ang) + 1) for ang, f_i in zip(angle, flo_imp)])
            for angle, flo_imp in zip(phi, floor_impe)
        ]
    )  # array size nsrc,nrec,freq
    # numerical distance
    w = np.sqrt(
        np.array(
            [
                np.array(
                    [
                        complex(0, 0.5) * pfc * dc * (np.sin(ang) + 1 / f_i) ** 2
                        for ang, f_i, dc in zip(angle, flo_imp, dc_s)
                    ]
                )
                for angle, flo_imp, dc_s in zip(phi, floor_impe, dist_chemin)
            ]
        )
    )
    erfct = np.exp(-1 * w**2) * erfc(complex(0, -1) * w)
    Fw = 1 + complex(0, np.sqrt(PI)) * w * erfct  # array size nsrc,nrec,nfreq
    # Attenuation
    Q = Rp + (1 - Rp) * Fw
    return Q


def convtiers(si, sj, k, ri, rj):
    """!
    Lisse les valeurs sur les 31 tiers d'octave de TYMPAN
    @param si: spectre du chemin direct
    @param sj: spectre du chemin diffusé
    @param k: spectre du nombre d'onde
    @param ri: longueur du chemin diffusé
    @param rj: longueur du chemin direct
    """
    assert len(sj) == len(si)
    nbVal = len(si)

    s = np.zeros(nbVal)
    cosTemp = np.zeros(nbVal)

    dp6 = 2 ** (1 / 6)
    invdp6 = 1 / dp6
    dfSur2f = (dp6 - invdp6) / 2

    sTemp = k * (ri - rj)
    i = 0
    for val1, val2, val3 in zip(si, sj, sTemp):
        s[i] = phase(val1) - phase(val2)
        i += 1
    df = sqrt(1 + dfSur2f**2)
    for i in range(nbVal):
        cosTemp[i] = cos(sTemp[i] * df + s[i] - sTemp[i])
    sTemp = sTemp * dfSur2f
    for i in range(nbVal):
        s[i] = sin(sTemp[i]) / sTemp[i] * cosTemp[i]

    return s


def lisse(valeurs, frequences):
    """!
    Lisse les valeurs sur les 31 tiers d'octave de TYMPAN
    @param valeurs: les valeurs par fréquences du spectre a lisser
    @param frequences: les fréquences à considerer
    """
    assert len(frequences) == len(valeurs)
    assert np.all(sorted(frequences) == frequences)
    valeurs_phy = 10 ** (valeurs / 10)
    i = 0
    pre_i = i
    lissage = []
    while frequences[i] < INTER_FREQ[0]:
        i += 1
    for freq in INTER_FREQ[1:-1]:
        som = 0
        while frequences[i] <= freq:
            som += valeurs_phy[i]
            i += 1
        lissage.append(som / (i - pre_i))
        pre_i = i
    lissage.append(sum(frequences[i:]))
    lissage = 10 * np.log10(np.array(lissage))
    return lissage


def compare_tympan2csv(tympan_file, solver, param_file, csv_file, header):
    """!
    @param tympan_file String: Le projet tympan sur lequel calculere le resultat
    @param solver String: Le solver à utiliser pour les calculs
    @param param_file String: Le fichier de parametres a utiliser
    @param csv_file String: Le fichier csv a comparer au resultat tympan
    @param header String: Le nom de la colonne ou lire le niveau des récépteurs
    """
    res_ty = tympan_result(tympan_file, solver, param_file)
    res_ty = np.array([Spectrum(spec).dBLin for spec in res_ty])
    res_th = pd.read_csv(csv_file, sep=";")[header].values
    assert res_ty.shape == res_th.shape
    diff = res_ty - res_th
    return res_ty, res_th, diff
