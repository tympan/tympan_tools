#!/usr/bin/python
# -*- coding: utf-8 -*-
from tympan.models.project import Project
from tympan.models.solver import Model
from util_mat import FREQ_TABLE, solve_tympan_project
from collections import OrderedDict

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

ind_100 = np.where(FREQ_TABLE == 100)[0][0]
ind_500 = np.where(FREQ_TABLE == 500)[0][0]
ind_1000 = np.where(FREQ_TABLE == 1000)[0][0]

colors = ["blue", "green", "yellow", "red"]
nodes = [0.0, 0.33, 0.66, 1.0]  # Position of each color
cmap = LinearSegmentedColormap.from_list("custom_colormap", list(zip(nodes, colors)))

# Plot a color bar to display the colormap
gradient = np.linspace(0, 1, 100)
gradient = np.vstack((gradient, gradient))


def calcul_carto(tympan_file, solver, param_file):
    """
    calcul le résultat tympan et les coordonnées des récépteurs
    @param tympan_file String: Le fichier tympan pour la simulation
    @param solver String: Le nom du solver a utiliser
    @param param_file String: Le fichier de parametres a utiliser
    """
    proj = Project.from_xml(tympan_file, verbose=True, size_criterion=0.0, refine_mesh=True, use_vol_landtakes=False)
    model = Model.from_project(proj)
    pr, res = solve_tympan_project(proj)

    res_spectra = np.array([10 * np.log10(sum(rec) / 4e-10) for rec in res.spectra()], dtype=float)

    res_100 = OrderedDict()
    res_500 = OrderedDict()
    res_1000 = OrderedDict()
    for rec, result in zip(model.receptors, res_spectra):
        key = (round(rec.position.x, 5), round(rec.position.y, 5))
        res_100[key] = result[ind_100]
        res_500[key] = result[ind_500]
        res_1000[key] = result[ind_1000]

    return res_100, res_500, res_1000


def compare_plot(res_butte, res_const, freq):
    """
    affiche côte a côte la carto de la butte et celle de la construction
    @param res_butte : dict contenant les résultat du calcul sur la butte
    @param res_const : dict contenant les résultat du calcul sur la construction
    @param freq : valeur de la frequence considéré
    """
    title_butte = "Butte " + str(freq) + " Hz"

    ser_butte = pd.Series(list(res_butte.values()), index=pd.MultiIndex.from_tuples(res_butte.keys()))
    df_butte = ser_butte.unstack()
    map_butte = df_butte.to_numpy()
    plt.subplot(1, 2, 1)
    plt.matshow(map_butte, fignum=False, cmap=cmap, vmin=55, vmax=80)
    plt.colorbar()
    plt.title(title_butte)

    title_const = "Construction " + str(freq) + " Hz"
    ser_const = pd.Series(list(res_const.values()), index=pd.MultiIndex.from_tuples(res_const.keys()))
    df_const = ser_const.unstack()
    map_const = df_const.to_numpy()
    plt.subplot(1, 2, 2)
    plt.matshow(map_const, fignum=False, cmap=cmap, vmin=55, vmax=80)
    plt.title(title_const)
    title = "output/test10_" + str(freq) + ".jpg"
    plt.colorbar()
    plt.savefig(title)

    return map_butte, map_const


def main(tympan_files, solver_name="default", param_file="param_solver.ini", plot_mode=True):
    """!
    @param tympan_files array: Les fichiers pour la simulation
    @param solver_name String: Le solver utiliser
    @param param_file String: Le fichier de parametres a utiliser
    @param plot_mode bool: True si un affichage est requis
    """
    res_butte_100, res_butte_500, res_butte_1000 = calcul_carto(
        tympan_files[0], solver_name, param_file
    )
    res_const_100, res_const_500, res_const_1000 = calcul_carto(tympan_files[1], solver_name, param_file)

    if plot_mode is True:
        res_butte_100, res_const_100 = compare_plot(res_butte_100, res_const_100, 100)
        res_butte_500, res_const_500 = compare_plot(res_butte_500, res_const_500, 500)
        res_butte_1000, res_const_1000 = compare_plot(res_butte_1000, res_const_1000, 1000)

    return [res_butte_100.flatten(), res_butte_500.flatten(), res_butte_1000.flatten()], [
        res_const_100.flatten(),
        res_const_500.flatten(),
        res_const_1000.flatten(),
    ]
