#!/usr/bin/python
# -*- coding:utf-8 -*-
"""!
Case 3: Puissance accoustique d'un batiment industriel
"""
from bin.export_spectres_src import main as export_spectre

# default param for solver
default_file = r"param_solver.ini"
# default solver
default_solver = "default"


def main(tympan_file, res_file, solver_name=default_solver, param_file=default_file, plot_mode=True):
    """!
    @param tympan_file String: Le fichier tympan pour la simulation
    @param res_file String: Le fichier de resultat
    @param solver_name String: Le solver a utiliser
    @param param_file String: le fichier de parametres a utiliser
    @param plot_mode bool: True si un affichage est requis
    """
    export_spectre(tympan_file, res_file)
