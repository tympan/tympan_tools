#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Case 1 : Free field propagation calculation, test if an engine is source alike in distant field """
from tympan.models.project import Project
from tympan.models.solver import Model

from util_mat import calcul_celerity, attenuation, FREQ_TABLE, tympan_result
import numpy as np

import math
from math import sqrt
import matplotlib.pyplot as plt

plt.style.use("ggplot")

# SQRT(2)
SQ2 = sqrt(2)
# PI
PI = math.pi
ind = np.where(FREQ_TABLE == 1000)[0][0]


def calcul_SP_hemi(SP_model, lw=100, atm_pression=1.01325e5, temperature=50):
    """!
    Calculus of the theorical values for each receptor
    @param SP_model Model: The model to use as basis
    @param lw array like: The level for each frequence
    @param atm_pression float: The atmospheric pression
    @param temperature float: The temperature
    @out array like: The global value for each receptor"""
    i = 0
    r = np.empty(255, dtype=object)
    for rec in SP_model.receptors:
        if rec.position.y == 0:
            r[i] = rec.position.x
            i += 1
    r_demi = r - 0.5
    alpha = attenuation(100, temperature, atm_pression)
    rho, celerity = calcul_celerity(atm_pression, temperature)

    freq_power = 10 ** (lw / 10) * 1e-12  # array size 31
    power_hemi = np.array(
        [freq_power * rho * celerity / (2 * PI * d**2) for d in r_demi]
    )  # array size (nreceptors,31)
    level_hemi = np.array(
        [10 * np.log10(rec / 4e-10) - alpha * d for rec, d in zip(power_hemi, r_demi)]
    )  # array size (nreceptors, 31)
    return np.array([rec[ind] for rec in level_hemi])


def calcul_SP_omni(SP_model, lw=100, atm_pression=1.01325e5, temperature=50):
    """!
    Calculus of the theorical values for each receptor hemispherical case
    @param SP_model Model: The model to use as basis
    @param lw array like: The level for each frequence
    @param atm_pression float: The atmospheric pression
    @param temperature float: The temperature
    @out array like: The global value for each receptor"""
    i = 0
    r = np.empty(255, dtype=object)
    for rec in SP_model.receptors:
        if rec.position.y == 0:
            r[i] = rec.position.x
            i += 1
    alpha = attenuation(100, temperature, atm_pression)
    rho, celerity = calcul_celerity(atm_pression, temperature)

    freq_power = 10 ** (lw / 10) * 1e-12  # array size 31
    power_omni = np.array(
        [freq_power * rho * celerity / (4 * PI * d**2) for d in r]
    )  # array size (nreceptors,31)
    level_omni = np.array(
        [10 * np.log10(rec / 4e-10) - alpha * d for rec, d in zip(power_omni, r)]
    )  # array size (nreceptors, 31)
    return np.array([rec[ind] for rec in level_omni])


def main(
    SP_hemi, SP_omni, machine, face, solver_name="default", param_file="param_solver.ini", plot_mode=True
):
    """!
    Plot the différents level for the frequence freq for the different files
    @param SP_hemi String:file for the punctual source with hemispheric radiation
    @param SP_omni String:file for the punctual source with omnidirectional radiation
    @param machine String:file for the engine case
    @param face String:file the radiating face case
    """
    # index 510 to 765 selects results with y=0
    tympan_hemi = [res[ind] for res in tympan_result(SP_hemi, solver_name, param_file)][510:765]
    tympan_omni = [res[ind] for res in tympan_result(SP_omni, solver_name, param_file)][510:765]
    tympan_machine = [res[ind] for res in tympan_result(machine, solver_name, param_file)][510:765]
    tympan_face = [res[ind] for res in tympan_result(face, solver_name, param_file)][510:765]

    level_hemi = []
    level_omni = []
    SP_hemi_model = Model.from_project(Project.from_xml(SP_hemi))
    SP_omni_model = Model.from_project(Project.from_xml(SP_omni))

    level_hemi = calcul_SP_hemi(SP_hemi_model)
    level_omni = calcul_SP_omni(SP_omni_model)
    r = np.array([np.sqrt((rec.position.x) ** 2 + (rec.position.y) ** 2) for rec in SP_omni_model.receptors])[
        510:765
    ]

    if plot_mode:
        plt.figure()
        plt.title("Cas d'une source omnispherique")
        plt.plot(r, tympan_machine, color="red", label="machine tympan cubique")
        plt.plot(r, level_omni, color="blue", label="calcul analytique")
        plt.plot(r, tympan_omni, color="black", label="source tympan omnidirectionelle")
        plt.legend(loc="best")
        plt.savefig("output/test1_omni.jpg")
        plt.figure()
        plt.title("Cas d'une source hemispherique")
        plt.plot(r, tympan_face, color="red", label="face tympan rayonnante")
        plt.plot(r, level_hemi, color="blue", label="calcul analytique")
        plt.plot(r, tympan_hemi, color="black", label="source tympan hemispherique")
        plt.legend(loc="best")
        plt.savefig("output/test1_hemi.jpg")
    return tympan_hemi, tympan_omni, tympan_face, tympan_machine, level_omni, level_hemi
