import xml.etree.ElementTree as ET
import os


def density_count(file_path):
    tree = ET.parse(file_path)
    root = tree.getroot()
    list_density_h = root.findall(".//densiteSrcsH")
    list_density_v = root.findall(".//densiteSrcsV")
    dico_h = get_dico_from_list(list_density_h)
    dico_v = get_dico_from_list(list_density_v)
    return dico_h, dico_v


def get_dico_from_list(l):
    dico = {}
    for elt in l:
        if elt.text in dico.keys():
            dico[elt.text] += 1
        else:
            dico[elt.text] = 1
    return dico


base_dir = 'R:/inge.001/THF/CCG Bouchain/TYMPAN/2016/MODELE'
(_, _, filenames) = next(os.walk(base_dir))
dico = {}
for filename in filenames:
    name, extension = os.path.splitext(filename)
    if extension == '.xml':
        # print('Nom : ', name, ' Extension : ', extension)
        dico_h, dico_v = density_count(os.path.join(base_dir, filename))
        if len(dico_h) != 0 or len(dico_v) != 0:
            dico[filename] = [filename, dico_h, dico_v]
print(dico)