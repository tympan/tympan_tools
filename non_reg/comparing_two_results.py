import argparse
import pandas as pd


def main(csv_results_file1, csv_results_file2, output_file_name):
    dataframe1 = pd.read_csv(csv_results_file1, delimiter=';',
                             skiprows=3, encoding='latin1')
    dataframe2 = pd.read_csv(csv_results_file2, delimiter=';',
                             skiprows=3, encoding='latin1')
    dataframe2['LW2'] = dataframe2['LW']
    dataframe1['LW2'] = pd.Series([0 for x in range(len(dataframe1.index))], index=dataframe1.index)
    dataframe2 = dataframe2[dataframe1.columns.tolist()]
    diff = dataframe2.set_index('dBA').subtract(dataframe1.set_index('dBA'))

    writer_orig = pd.ExcelWriter(
        output_file_name + '.xlsx', engine='xlsxwriter')
    diff.to_excel(writer_orig, index=True, sheet_name='comparaison')
    workbook = writer_orig.book
    format_red = workbook.add_format({'bg_color': '#FFC7CE',
                                      'font_color': '#9C0006'})
    format_green = workbook.add_format({'bg_color': '#C6EFCE',
                                        'font_color': '#006100'})
    format_yellow = workbook.add_format({'bg_color': '#FFFF00',
                                         'font_color': '#808000'})
    format_black = workbook.add_format({'bg_color': '#D3D3D3',
                                        'font_color': ' #A9A9A9'})
    worksheet = writer_orig.sheets['comparaison']
    worksheet.conditional_format(1, 1, len(diff.index), len(diff.columns), {'type': 'cell',
                                                                            'criteria': 'greater than',
                                                                            'value': 10,
                                                                            'format': format_black})
    worksheet.conditional_format(1, 1, len(diff.index), len(diff.columns), {'type': 'cell',
                                                                            'criteria': 'less than',
                                                                            'value': -10,
                                                                            'format': format_black})
    worksheet.conditional_format(1, 1, len(diff.index), len(diff.columns), {'type': 'cell',
                                                                            'criteria': 'between',
                                                                            'minimum': 3,
                                                                            'maximum': 10,
                                                                            'format': format_red})
    worksheet.conditional_format(1, 1, len(diff.index), len(diff.columns), {'type': 'cell',
                                                                            'criteria': 'between',
                                                                            'minimum': -10,
                                                                            'maximum': -3,
                                                                            'format': format_red})
    worksheet.conditional_format(1, 1, len(diff.index), len(diff.columns), {'type': 'cell',
                                                                            'criteria': 'between',
                                                                            'minimum': 1,
                                                                            'maximum': 3,
                                                                            'format': format_yellow})
    worksheet.conditional_format(1, 1, len(diff.index), len(diff.columns), {'type': 'cell',
                                                                            'criteria': 'between',
                                                                            'minimum': -3,
                                                                            'maximum': -1,
                                                                            'format': format_yellow})
    worksheet.conditional_format(1, 1, len(diff.index), len(diff.columns), {'type': 'cell',
                                                                            'criteria': 'between',
                                                                            'minimum': -1,
                                                                            'maximum': 1,
                                                                            'format': format_green})

    writer_orig.save()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Comparaison de deux fichiers de resultats Code_TYMPAN')
    parser.add_argument(
        'tympan_csv_file1',
        help='le premier fichier csv de tympan - version n')
    parser.add_argument(
        'tympan_csv_file2',
        help='le second fichier csv de tympan - version n+1')
    parser.add_argument(
        'output_file_name',
        help='le fichier de comparaison entre les 2 versions')
    args = parser.parse_args()
    main(args.tympan_csv_file1, args.tympan_csv_file2, args.output_file_name)
