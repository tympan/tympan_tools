"""
Compare multiple project results of 2 versions
"""
import os
import os.path as osp

from comparing_two_results import main as compare_two_results

# Environment variables.

_HERE = osp.realpath(osp.dirname(__file__))
VERSION_N = "4.2.1"
VERSION_N_PLUS_1 = "4.3.0_dev"
MODEL_DIR = "C:\\projects\\tympan_tools\\non_reg\\input"
OUTPUT_DIR = "C:\\projects\\tympan_tools\\non_reg\\output"


def list_model_input(input_dir):
    """
    Revoie les modèles industriels en entrée
    :return:
    """
    files = [f for f in os.listdir(input_dir) if osp.isfile(osp.join(input_dir, f)) and f.endswith("xml")]
    return files


if __name__ == '__main__':
    my_files = list_model_input(MODEL_DIR)
    for file in my_files:
        result_file_1_name = "resultats_" + VERSION_N.replace(".", "") + "_" + osp.splitext(file)[0] + ".csv"
        result_file_2_name = "resultats_" + VERSION_N_PLUS_1.replace(".", "") + "_" + osp.splitext(file)[0] + ".csv"
        result_file_1_path = osp.join(OUTPUT_DIR, VERSION_N, result_file_1_name)
        result_file_2_path = osp.join(OUTPUT_DIR, VERSION_N_PLUS_1, result_file_2_name)
        output_file_name = "comp_" + VERSION_N_PLUS_1.replace(".", "") + "_" + VERSION_N.replace(".", "") + "_"\
                           + osp.splitext(file)[0]
        output_file_path = osp.join(OUTPUT_DIR, "comp", output_file_name)
        compare_two_results(result_file_1_path, result_file_2_path, output_file_path)
