"""Ease paths configuration for test in Python

Allow to access the test data, solvers and other resources in a cross
platform way (the relative path to use between a test executable and
the resources is not the same on Linux and Windows) by exporting the
following constants:

PROJECT_BASE
    absolute path to the root of the Code_TYMPAN build tree

TEST_DATA_DIR
    absolute path to the directory holding the test data

TEST_SOLVERS_DIR
    absolute path to the directory holding the solvers
"""

import argparse
from pathlib import Path
import os
import os.path as osp

import numpy as np

from tympan.models.project import Project
from tympan.models.solver import Model, Solver

# Environment variables.

_HERE = osp.realpath(osp.dirname(__file__))
PROJECT_BASE = osp.abspath(osp.join(_HERE, "..", ".."))

TEST_SOLVERS_DIR = os.environ.get("TYMPAN_SOLVERDIR")
if not TEST_SOLVERS_DIR:
    raise RuntimeError("The test solver plugins dir wasn't found")


# Utilities.

def compare_floats(x, y):
    """Compare x and y which are float arrays:
    Go through them, considering x[i] and y[i] equal up to a 3 decimal
    precision. Then when they sufficiently differ, return -1 if x[i] < y[i]
    and 1 otherwise. 0 is returned if the arrays are equal.
    """
    for xi, yi in zip(x, y):
        if not np.allclose(xi, yi, atol=1e-03):
            if xi < yi:
                return -1
            else:
                return 1
    return 0  # arrays are equal


def _solve_with_file(file, input_dir, output_dir, version):
    """Run a tympan simulation on project `file` and return the results
    """
    # Load and solve the project
    project = Project.from_xml(osp.join(input_dir, file), verbose=True)
    model = Model.from_project(project)
    solver = Solver.from_project(project, TEST_SOLVERS_DIR, verbose=True)
    # Fix issue #155 - python tests using solver timing out with nb_threads = 1
    solver.nb_threads = 4
    solver_result = solver.solve(model)
    project.import_result(model, solver_result)
    current_result = project.current_computation.result
    output_file_path = osp.join(output_dir, version, "resultats_" + version.replace(".", "") + "_"
                                + osp.splitext(file)[0] + ".csv")
    current_result.saveValue(output_file_path, 0, 100)


def list_model_input(input_dir):
    """
    Revoie les modèles industriels en entrée
    :return:
    """
    files = [f for f in os.listdir(input_dir) if osp.isfile(osp.join(input_dir, f)) and f.endswith("xml")]
    return files


def parse_args():
    """
    Fonction permettant de gérer les fichiers de config et le mode du calcul
    :return:
    """
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("-i", "--input-dir", help="input repository",
                        default=os.getcwd())
    parser.add_argument("-o", "--output-dir", help="output repository",
                        default=os.getcwd())
    parser.add_argument("-v", "--version", help="version de Code_TYMPAN")
    args = parser.parse_args()
    # check arguments
    input_path = Path(args.input_dir)
    if not input_path.exists():
        raise IOError("Directory {} doesn't exists".format(input_path))

    output_path = Path(args.output_dir)
    if not output_path.exists():
        raise IOError("Directory {} doesn't exists".format(output_path))

    version = args.version
    if version is None:
        raise ValueError("version must be defined")

    return input_path, output_path, version


if __name__ == '__main__':
    # parse arguments
    my_input_dir, my_output_dir, my_version = parse_args()
    results = {}
    files = list_model_input(my_input_dir)
    for file in files:
        try:
            _solve_with_file(file, my_input_dir, my_output_dir, my_version)
        except:
            print("Error computing Tympan solve on {}", file)
