rem Script pour lancer les tests de non régression sur les modèles des tests insdustriels ou sur les modèles utilisateurs
rem Doit être configuré et lancé sur une distribution de Code_TYMPAN installée sur le PC de tests

set TYMPAN_INSTALL_PATH=C:\Code_TYMPAN_Dev

set INPUT_DIR=C:\projects\tympan_tools\non_reg\input
set OUTPUT_DIR=C:\projects\tympan_tools\non_reg\output
set VERSION=4.3.0_dev

set errorlevel = 0

rem use legacy windows stdio to prevent the TYMPAN API from crashing/hanging when redirecting the output
rem with _stdout_redirected (e.g. when calling Project.from_xml with verbose=False)
set PYTHONLEGACYWINDOWSSTDIO=1
set PYTHONIOENCODING=UTF8

rem Activer l'environnement virtuel
rem :LBL0
rem call %PYTHON_ENV%\Scripts\activate.bat
rem if %errorlevel%==0 goto LBL1
rem echo "Erreur bloquante lors de l'activation de l'environnement virtuel Python"
rem pause
rem exit

rem Positionner les variables d'environnement pour les tests unitaires
:LBL1
rem call SetEnvTympanTests.bat
set PYTHONTYMPAN=%TYMPAN_INSTALL_PATH%\Python310
set TYMPAN_SOLVERDIR=%TYMPAN_INSTALL_PATH%\plugins
set CGAL_BINDINGS_PATH=%TYMPAN_INSTALL_PATH%\cython\CGAL
set TYMPAN_PYTHON_INTERP=%PYTHONTYMPAN%\python.exe
set PYTHONPATH=%TYMPAN_INSTALL_PATH%\cython;%PYTHONTYMPAN%\DLLs;%PYTHONTYMPAN%\Lib;%PYTHONTYMPAN%\Lib\site-packages;%TYMPAN_INSTALL_PATH%
set PATH=%TYMPAN_INSTALL_PATH%;%PYTHONTYMPAN%\;%PYTHONTYMPAN%\DLLs;%PYTHONTYMPAN%\Lib;%PYTHONTYMPAN%\Scripts
set LD_LIBRARY_PATH=%TYMPAN_INSTALL_PATH%\Lib;%TYMPAN_INSTALL_PATH%\cython;%TYMPAN_INSTALL_PATH%\cython\CGAL

if %errorlevel%==0 goto LBL2
echo "Erreur bloquante lors du positionnement des variables d'environnement"
pause
exit

rem Lancement des tests unitaires en mode Release
:LBL2
%PYTHONTYMPAN%\python loop_solve.py -i %INPUT_DIR% -o %OUTPUT_DIR% -v %VERSION%
if %errorlevel%==0 goto FIN
echo "Erreur bloquante lors du lancement du script python"
pause
exit

:FIN

pause
