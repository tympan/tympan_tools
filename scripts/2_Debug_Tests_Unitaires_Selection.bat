rem Script pour lancer les tests unitaires Tympan en mode debug
rem Permet d'automatiser la section correspondante de la documentation développeur

set errorlevel = 0

set TYMPAN_BUILD_PATH=c:\projects\code_tympan_build_d
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install_d
set PYTHON_ENV=C:\dists\python\venv312tympan

cd %TYMPAN_BUILD_PATH%

rem Activer l'environnement virtuel
:LBL0
call %PYTHON_ENV%\Scripts\activate.bat
if %errorlevel%==0 goto LBL1
echo "Erreur bloquante lors de l'activation de l'environnement virtuel Python"
pause
exit

rem Positionner les variables d'environnement pour les tests unitaires
:LBL1
call SetEnvTympanTests.bat
set TYMPAN_PYTHON_INTERP=%PYTHON_ENV%\Scripts\python.exe

rem Décommenter pour lancer les tests visuels; requiert l'installation du package Python 'descartes'
rem set RUN_VISUAL_TESTS=ON

if %errorlevel%==0 goto LBL2
echo "Erreur bloquante lors du positionnement des variables d'environnement pour les tests"
pause
exit

rem Lancement des tests unitaires en mode Debug
:LBL2
ctest -C Debug -R test_altimetry_envconfig --output-on-failure -V
rem python python\tests\test_sources_receptors.py

if %errorlevel%==0 goto FIN
echo "Erreur bloquante lors du lancement des tests unitaires en mode Debug"
pause
exit

:FIN

pause
