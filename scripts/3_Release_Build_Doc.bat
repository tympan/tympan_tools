rem Script pour builder la documentation Tympan en mode Release

set errorlevel = 0

set TYMPAN_BUILD_PATH=c:\projects\code_tympan_build_doc
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install_doc
set PYTHON_ENV=C:\dists\python\venv312tympan

set CGAL_PATH=C:\dists\CGAL-5.6.1
set CGAL_DIR=%CGAL_PATH%

set PATH=%CGAL_PATH%\auxiliary\gmp\lib;%CGAL_PATH%\auxiliary\gmp\include;C:\Qt\6.5.3\msvc2019_64\bin;C:\Qt\6.5.3\msvc2019_64\plugins\platforms;%PATH%

cd %TYMPAN_BUILD_PATH%

rem Activer l'environnement virtuel
:LBL0
call %PYTHON_ENV%\Scripts\activate.bat
if %errorlevel%==0 goto LBL1
echo "Erreur bloquante lors de l'activation de l'environnement virtuel Python"
pause
exit

rem Exéuction de la tâche doc par CMake en Release
:LBL1
cmake ..\code_tympan -G "Visual Studio 17 2022" -DCMAKE_GENERATOR_TOOLSET=v142 -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=C:\Qt\Qt6.5.3\msvc2019_64\lib\cmake -DCMAKE_INSTALL_PREFIX=%TYMPAN_INSTALL_PATH% -DCGAL_DIR=%CGAL_DIR% -A x64 >> output_conf_file.txt 2>&1
call SetEnvTympanTests.bat
cmake --build . --target doc --config Release >> log_build_doc.txt 2>&1
if %errorlevel%==0 goto FIN
echo "Erreur bloquante lors du lancement de la tâche doc par CMake en Release"
pause
exit

:FIN
echo "Build de la documentation de Code_TYMPAN en Release réussi"

pause
