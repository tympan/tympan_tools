rem Script pour builder Tympan en mode Release
rem Permet d'automatiser la section Build en Release de la documentation développeur

set errorlevel = 0

set TYMPAN_BUILD_PATH=c:\projects\code_tympan_build
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install
set PYTHON_ENV=C:\dists\python\venv312tympan

set CGAL_PATH=C:\dists\CGAL-5.6.1
set CGAL_DIR=%CGAL_PATH%

set PATH=%CGAL_PATH%\auxiliary\gmp\lib;%CGAL_PATH%\auxiliary\gmp\include;C:\Qt\6.8.2\msvc2022_64\bin;C:\Qt\6.8.2\msvc2022_64\plugins\platforms;%PATH%

cd %TYMPAN_BUILD_PATH%

rem Activer l'environnement virtuel
:LBL0
call %PYTHON_ENV%\Scripts\activate.bat
if %errorlevel%==0 goto LBL1
echo "Erreur bloquante lors de l'activation de l'environnement virtuel Python"
pause
exit /B 1

rem Construire le système de Build en Release
:LBL1
cmake ..\code_tympan -G "Visual Studio 17 2022" -DCMAKE_GENERATOR_TOOLSET=v143 -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=C:\Qt\Qt6.8.2\msvc2022_64\lib\cmake -DCMAKE_INSTALL_PREFIX=%TYMPAN_INSTALL_PATH% -DCGAL_DIR=%CGAL_DIR% -A x64 >> output_conf_file.txt 2>&1
if %errorlevel%==0 goto LBL2
echo "Erreur bloquante lors de la construction du système de build par CMake"
pause
exit /B 1

rem Exécution de la tâche install par CMake en Release
:LBL2
cmake --build . --target install --config Release >> output_build_file.txt 2>&1
if %errorlevel%==0 goto FIN
echo "Erreur bloquante lors du lancement de la tâche install par CMake en Release"
pause
exit /B 1

:FIN
echo "Compilation et installation de Code_TYMPAN en Release réussie"

pause
exit /B 0
