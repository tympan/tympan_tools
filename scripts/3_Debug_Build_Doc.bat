rem Script pour builder la documentation Tympan en mode Debug

set errorlevel = 0

set TYMPAN_BUILD_PATH=c:\projects\code_tympan_build_d
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install_d
set PYTHON_ENV=C:\dists\python\venv312tympan

cd %TYMPAN_BUILD_PATH%

rem Activer l'environnement virtuel
:LBL0
call %PYTHON_ENV%\Scripts\activate.bat
if %errorlevel%==0 goto LBL1
echo "Erreur bloquante lors de l'activation de l'environnement virtuel Python"
pause
exit

rem Exéuction de la tâche doc par CMake en Debug
:LBL1
call SetEnvTympanTests.bat
cmake --build . --target doc --config Debug
if %errorlevel%==0 goto FIN
echo "Erreur bloquante lors du lancement de la tâche doc par CMake en Debug"
pause
exit

:FIN
echo "Build de la documentation de Code_TYMPAN en Debug réussi"

pause
