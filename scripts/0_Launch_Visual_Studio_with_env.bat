﻿rem Script pour lancer Visual Studio avec les variables d'environnement Tympan mises en place
rem Permet entre autres de debugger les tests unitaires GTest

set REPERTOIRE_TRAVAIL="C:\projects\tympan\scripts"

set TYMPAN_BUILD_PATH="c:\projects\code_tympan_build_d"
set TYMPAN_INSTALL_PATH="c:\projects\code_tympan_install_d"
set PYTHON_ENV="C:\dists\python\venv312tympan"

set CGAL_DIR=C:\dists\CGAL-5.5.1

set PATH=C:\Qt\5.15.2\msvc2019_64\bin;C:\Qt\5.15.2\msvc2019_64\plugins\platforms;%PATH%

cd %REPERTOIRE_TRAVAIL%

rem Lancement du fichier de commande SetEnvTympantests.bat
rem Bien vérifier si le contenu pointe bien sur les chemins en Debug
call %TYMPAN_BUILD_PATH%\SetEnvTympanTests.bat
rem Il est nécessaire de setter la variable TYMPAN_PYTHON_INTERP pour que Tympan situe l'interpréteur Python à utiliser.
set TYMPAN_PYTHON_INTERP=%PYTHON_ENV%\Scripts\python.exe

rem Pour le Debug, la variable TYMPAN_SOLVERDIR n'est pas utile
rem set TYMPAN_SOLVERDIR=%INSTALL%\pluginsd

rem Définition de TYMPAN_DEBUG pour debugger l'exécution du Solve
set TYMPAN_DEBUG=keep_tmp_files;interactive;monothread

rem Activer l'environnement virtuel
rem call %PYTHON_ENV%\Scripts\activate.bat

rem Lancer Visual Studio 2019 avec les variables d'environnement correctement positionnées (après les comandes Set)
"C:\Program Files\Microsoft Visual Studio\2022\Professional\Common7\IDE\devenv.exe"
