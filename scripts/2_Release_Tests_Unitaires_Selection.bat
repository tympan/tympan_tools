rem Script pour lancer les tests unitaires Tympan en mode Release
rem Permet d'automatiser la section correspondante de la documentation développeur

set errorlevel = 0

set TYMPAN_BUILD_PATH=c:\projects\code_tympan_build
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install
set PYTHON_ENV=C:\dists\python\venv312tympan

cd %TYMPAN_BUILD_PATH%

rem Activer l'environnement virtuel
:LBL0
call %PYTHON_ENV%\Scripts\activate.bat
if %errorlevel%==0 goto LBL1
echo "Erreur bloquante lors de l'activation de l'environnement virtuel Python"
pause
exit

rem Positionner les variables d'environnement pour les tests unitaires
:LBL1
call SetEnvTympanTests.bat
set TYMPAN_PYTHON_INTERP=%PYTHON_ENV%\Scripts\python.exe

rem Décommenter pour lancer les tests visuels; requiert l'installation du package Python 'descartes'
rem set RUN_VISUAL_TESTS=ON

if %errorlevel%==0 goto LBL2
echo "Erreur bloquante lors du positionnement des variables d'environnement pour les tests"
pause
exit

rem Lancement des tests unitaires en mode Release
:LBL2
ctest -C Release -I 54,63 --output-on-failure
rem ctest -C Release -R test_altimetry --output-on-failure
rem ctest -C Release -R test_move_receptor_position --repeat until-fail:100 --output-on-failure
rem ctest -C Release -I 51,51 --output-on-failure
rem ctest -C Release -I 56,96 -V

if %errorlevel%==0 goto FIN
echo "Erreur bloquante lors du lancement des tests unitaires en mode Release"
pause
exit

:FIN

pause
