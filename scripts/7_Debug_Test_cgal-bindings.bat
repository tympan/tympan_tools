rem Script pour tester le bon fonctionnement de cgal-bindings en debug
rem Mise en place del'environnement comme dans Tympan python
rem Puis test d'une triangulation de Delaunay

set REPERTOIRE_TRAVAIL="C:\projects\tympan_tools\scripts"
set TYMPAN_BUILD_PATH=c:\projects\code_tympan_build_d
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install_d
set PYTHON_ENV=C:\dists\python\venv312tympan

rem Aller sous le répertoire de BUILD
cd %TYMPAN_BUILD_PATH%

call SetEnvTympanTests.bat
rem Activer l'environnement virtuel
call %PYTHON_ENV%\Scripts\activate.bat

rem Lancer le script de test de triangulation de Delaunay
python %REPERTOIRE_TRAVAIL%\7_Debug_Test_cgal-bindings.py
pause
