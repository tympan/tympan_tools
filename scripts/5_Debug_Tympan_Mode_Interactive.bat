rem Script pour lancer Tympan en debug en mode 'Interactive'
rem Ce mode lancer le script de l'API Python solve_tympan_project
rem Il permet de debugger l'exécution du Solver en C++ avec Visual Studio

set REPERTOIRE_TRAVAIL="C:\projects\tympan\scripts"

set MODEL="C:\projects\tympan\issues\180\model.xml"
set RESULT="C:\projects\tympan\issues\180\result.xml"
set MESH="C:\projects\tympan\issues\180\mesh.ply"
set SIZE_CRITERION=0.0
set REFINE_MESH=True
set USE_VOLUME_LANDTAKES=False

set BUILD_INSTALL_PATH=c:\projects\code_tympan_build_d
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install_d
set PYTHON_ENV=C:\dists\python\venv312tympan

cd %REPERTOIRE_TRAVAIL%

rem Lancement du fichier de commande SetEnvTympantests.bat
rem Bien vérifier si le contenu pointe bien sur les chemins en Debug
call %BUILD_INSTALL_PATH%\SetEnvTympanTests.bat

rem Il est nécessaire de setter la variable TYMPAN_PYTHON_INTERP pour que Tympan situe l'interpréteur Python à utiliser.
set TYMPAN_PYTHON_INTERP=%PYTHON_ENV%\Scripts\python.exe

rem Pour le Debug, la variable TYMPAN_SOLVERDIR n'est pas utile
rem set TYMPAN_SOLVERDIR=%INSTALL%\pluginsd

rem Définition de TYMPAN_DEBUG pour debugger l'exécution du Solve
set TYMPAN_DEBUG=keep_tmp_files;interactive;monothread

rem Activer l'environnement virtuel
call %PYTHON_ENV%\Scripts\activate.bat

python %TYMPAN_INSTALL_PATH%\bin\solve_tympan_project.py %MODEL% %RESULT% %MESH% %TYMPAN_INSTALL_PATH%\pluginsd %SIZE_CRITERION% %REFINE_MESH% %USE_VOLUME_LANDTAKES%
pause
