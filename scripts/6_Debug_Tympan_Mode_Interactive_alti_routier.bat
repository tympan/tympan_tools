rem Script pour lancer le script python d'altimétrie du routier développé pour Cerema

set REPERTOIRE_TRAVAIL="C:\projects\tympan\scripts"

set MODEL="C:\projects\tympan\issues\test_altimetrie_routier\model.xml"
set FOLDER_RESULT="C:\projects\tympan\issues\test_altimetrie_routier"
set ROAD_PROFILES="C:\projects\tympan\issues\test_altimetrie_routier\test_route_droite_1.csv"
rem set MESH="C:\projects\tympan\issues\173\mesh.ply"
rem set SIZE_CRITERION=0.0
rem set REFINE_MESH=True
rem set USE_VOLUME_LANDTAKES=False

set TYMPAN_BUILD_PATH=c:\projects\code_tympan_build_d
set TYMPAN_INSTALL_PATH=c:\projects\code_tympan_install_d
set PYTHON_ENV=C:\dists\python\venv312tympan

cd %REPERTOIRE_TRAVAIL%

rem Lancement du fichier de commande SetEnvTympantests.bat
rem Bien vérifier si le contenu pointe bien sur les chemins en Debug
call %TYMPAN_BUILD_PATH%\SetEnvTympanTests.bat

rem Il est nécessaire de setter la variable TYMPAN_PYTHON_INTERP pour que Tympan situe l'interpréteur Python à utiliser.
set TYMPAN_PYTHON_INTERP=%PYTHON_ENV%\Scripts\python.exe

rem Pour le calcul d'altimétrie routier, la variable TYMPAN_SOLVERDIR est pour l'instant nécessaire
set TYMPAN_SOLVERDIR=%TYMPAN_INSTALL_PATH%\pluginsd

rem Définition de TYMPAN_DEBUG pour debugger l'exécution du Solve
set TYMPAN_DEBUG=keep_tmp_files;interactive;monothread

rem Activer l'environnement virtuel
call %PYTHON_ENV%\Scripts\activate.bat

python %TYMPAN_INSTALL_PATH%\bin\script_road_profile.py %MODEL% %ROAD_PROFILES% -t %FOLDER_RESULT%
pause
