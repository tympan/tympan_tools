rem Script pour lancer la tache de "lint" sur Tympan
rem Permet d'automatiser la section Lint de la documentation developpeur

set errorlevel = 0

set TYMPAN_BUILD_PATH=C:\projects\code_tympan_build_d
set PYTHON_ENV=C:\dists\python\venv312tympan

cd %TYMPAN_BUILD_PATH%

rem Activer l'environnement virtuel
:LBL0
call %PYTHON_ENV%\Scripts\activate.bat
if %errorlevel%==0 goto LBL1
echo "Erreur bloquante lors de l'activation de l'environnement virtuel Python"
pause
exit /B 1

rem Exécution de la tâche lint par CMake
:LBL1
cmake --build . --target lint
if %errorlevel%==0 goto FIN
echo "Erreur bloquante lors du lancement de la tache lint par CMake"
pause
exit /B 1

:FIN
echo "Tache Lint sur Code_TYMPAN réussie"

pause
exit /B 0
